# Centralized Health Camp Management System
An application  for immediate treatment in emergency situations via centrally managed health camps. <br>
- To direct a person to the most viable Health Centre in case of a
disaster.
- Inter – camp communication for load balancing.
- Display of real time status of resource availability and occupancy
and suggest authorities to build new facilities if the load in the
existing ones is too high.

# Assumptions
1. Location and occupancy of different hospital and data about patients is given so that we can map and/or divert the person to other hospital.
2. Optimization in regard to suggesting the patient to go to which hospital.
3. The load balancing will take place after proper communication between the the sending hospital and receiving hospital after taking due consent of the patient.

# Design Considerations
https://docs.google.com/document/d/12VRir9fiaw-taszbOnReT3ZhShPM6BagI7104YI4EjM/edit?usp=sharing

# Improvements
1. Try Clustering the hospitals on the basis of proximity of distance. So, that patient can be directed to the nearest hospital even if the hospital is in another state/district.
2. Trying hierarchical clustering to see which level of hierarchy fits the best in the application.
