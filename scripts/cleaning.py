# CLEANING THE DATA
infile = open("data/nin-health-facilities.csv",'r',encoding="cp850")
outfile = open("data/cleaned_data/nin-health-facilities-cleaned.csv",'a')
for line in infile :
    line= line.replace("&#34;","") # Web based unicode double quotes(")
    line= line.replace("&#39;","") # Web based unicode single quotes(')
    line= line.replace(";","")
    outfile.write(line)
infile.close()
outfile.close()