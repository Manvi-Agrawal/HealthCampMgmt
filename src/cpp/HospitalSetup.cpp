#include <omp.h>
#include <bits/stdc++.h>
#include<random>
#define INF 1000000000

using namespace std;

/*
Compile the code
g++ -std=c++11 HospitalSetup.cpp -fopenmp -o hs

Run the code
./hs <population_size>
*/

	struct chromosome
	{
		int number_of_beds;
		int rating;
	};

	struct weights
	{
		float weight_for_number_of_beds;
		float weight_for_rating;
	};


	const int MIN_BEDS = 50;
	const int MAX_BEDS = 200;
	const int MIN_RATING = 1;
	const int MAX_RATING = 10;

	class GeneticAlgorithm{

	public:
		GeneticAlgorithm(int _population_size){ population_size = _population_size;}
		void run();

	private:
		vector<chromosome> population;
		int population_size, number_of_generations;	
		float mu;
		weights weight_properties, alpha;
		
		void set_parameters();/*To take input from the user */
		void generate_initial_population();
		void crossover();
		void mutation();
		void selection();
		void print_population();

		float calculate_fitness(chromosome &hospital);
		bool compare_fitness(chromosome &a, chromosome &b);	
	};

	//Driver Code
	int main(int argc, char* argv[])
	{
		if(argc!=2)
		{
			cout << "Program requires population_size as a cmd line argument\n";
		}
		else
		{
			int population_size=stoi(argv[1]);
			GeneticAlgorithm ga = GeneticAlgorithm(population_size);
			ga.run();
		}
	return 0;
	}

	void GeneticAlgorithm :: set_parameters()
	{

		cout << "Enter the number of generations : " ;
		cin >> number_of_generations;
		 
		cout << "Enter the value of mu : " ;
		cin >> mu;

		cout << "Enter the value of alpha for number of beds and rating: " ;
		cin >> alpha.weight_for_number_of_beds >> alpha.weight_for_rating ;

		cout << "Enter the value of weights for number of beds and rating: " ;
		cin >> weight_properties.weight_for_number_of_beds >> weight_properties.weight_for_rating ;

		// Set size of population matrix
		population = vector<chromosome>(population_size);
	}

	void GeneticAlgorithm :: generate_initial_population()
	{
		#pragma omp parallel for
		for(int i=0; i<population_size; i++)
		{
			/*rand() generates a pseudo-random number between 0 and RAND_MAX*/
			population[i].number_of_beds = MIN_BEDS + rand()% (MAX_BEDS - MIN_BEDS + 1);
			population[i].rating = MIN_RATING + rand()% (MAX_RATING - MIN_RATING + 1);
		}
	}

	float GeneticAlgorithm :: calculate_fitness(chromosome &hospital)
	{

		float fitness_score = weight_properties.weight_for_number_of_beds * hospital.number_of_beds + weight_properties.weight_for_rating * hospital.rating ;
		return fitness_score;
	}

	bool GeneticAlgorithm :: compare_fitness(chromosome &a, chromosome &b)
	{
		return calculate_fitness(a) > calculate_fitness(b);
	}

	void GeneticAlgorithm :: selection()
	{
		using namespace std::placeholders;
	  	// Need to bind comparator if comaprator is member of the class
		sort(population.begin(),population.end(),bind(&GeneticAlgorithm::compare_fitness, this, _1, _2));
	}

	void GeneticAlgorithm :: crossover()
	{
		// Arithemetic crossover : a*x + (1-a)*y
		#pragma omp parallel for
		for(int i=0; i<population_size/2; i++)
		{
			int p1_number_of_beds = population[i].number_of_beds;
			int p2_number_of_beds = population[population_size-i].number_of_beds;
			int p1_rating = population[i].rating;
			int p2_rating = population[population_size-i].rating;

			population[i].number_of_beds = alpha.weight_for_number_of_beds * p1_number_of_beds + (1 - alpha.weight_for_number_of_beds) * p2_number_of_beds ;
			population[population_size-i].number_of_beds = alpha.weight_for_number_of_beds * p2_number_of_beds + (1 - alpha.weight_for_number_of_beds) * p1_number_of_beds ;
			population[i].rating = alpha.weight_for_rating * p1_rating + (1 - alpha.weight_for_rating) * p2_rating ;
			population[population_size-i].rating = alpha.weight_for_rating * p2_rating + (1 - alpha.weight_for_rating) * p1_rating ;
		}
	}

	void GeneticAlgorithm :: mutation()/*single point mutation*/
	{
		int num_mutation = mu * population_size;
		set<int> mutation_points = set<int>();
		while ( mutation_points.size() < num_mutation )
			mutation_points.insert( rand()%population_size );

		/*#pragma omp parallel for*/
		for(auto it=mutation_points.begin(); it!=mutation_points.end(); it++)
		{
			int i = *it;

			/*rand() generates a pseudo-random number between 0 and RAND_MAX*/
			population[i].number_of_beds = MIN_BEDS + rand()% (MAX_BEDS - MIN_BEDS + 1);
			population[i].rating = MIN_RATING + rand()% (MAX_RATING - MIN_RATING + 1);
		}
	}

	void GeneticAlgorithm :: print_population()
	{
		cout << "Number Of Beds\t" << "Rating\n" ;
		for(int i=0; i<population_size ; i++)
		{
			cout << population[i].number_of_beds << "\t" << population[i].rating << "\n";
		}
	}

	void GeneticAlgorithm :: run()
	{
		set_parameters();
		generate_initial_population();
		cout << "Initial population generated...\n";
		for(int gen=0; gen<number_of_generations; gen++)
		{
			selection();
			crossover();
			mutation();
			print_population();
			cout << "Generation : " << gen+1 << " completed\n";
		}
	}